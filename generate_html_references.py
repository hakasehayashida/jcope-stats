import bibtexparser
from datetime import datetime

# Input and output files
input_file = "jcope-stats.bib"
output_file = "public/jcope-publist.html"

# Load the BibTeX file
with open(input_file, "r") as bibtex_file:
    bib_database = bibtexparser.load(bibtex_file)

# Sort the entries by 'year' in ascending order
sorted_entries = sorted(
    bib_database.entries,
    key=lambda entry: int(entry.get('year', '0')),  # Default to 0 if no year
    reverse=True  # Descending order
)

# Start the HTML content
html_content = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sorted References</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        h1 {
            text-align: center;
        }
        h2 {
            color: #333;
            margin-top: 30px;
        }
        ul {
            list-style-type: none;
            padding: 0;
        }
        li {
            margin-bottom: 15px;
        }
        .reference {
            font-size: 1.1em;
        }
    </style>
</head>
<body>
    <h1>List of peer-reviewed publications using the JCOPE data</h1>
    <ul>
"""

# Initialize a variable to keep track of the last year added
last_year = None

# Format each entry and add to the HTML content
for entry in sorted_entries:
    # Extract information from the BibTeX entry
    author = entry.get('author', 'Unknown Author')
    year = entry.get('year', 'Unknown Year')
    title = entry.get('title', 'Untitled')
    journal = entry.get('journal', 'Unknown Journal')
    volume = entry.get('volume', '')
    pages = entry.get('pages', '')
    
    # Check if the year has changed, and add a new header if so
    if year != last_year:
        html_content += f"<h2>{year}</h2>"
        last_year = year
    
    # Build the citation string
    citation = f"{author} ({year}). <i>{title}</i>, <b>{journal}</b>, {volume}, {pages}."

    # Add the citation to the HTML list
    html_content += f"""
        <li>
            <div class="reference">{citation}</div>
        </li>
    """

# Close the HTML structure
html_content += """
    </ul>
</body>
</html>
"""

# Write the HTML content to the output file
with open(output_file, "w") as output_html:
    output_html.write(html_content)

print(f"Sorted references with year headers saved as '{output_file}'.")

