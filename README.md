# JCOPE-STATS

This notebook `jcope-stats.ipynb` generates two figures (Japanese and English versions) showing the annual count for the JCOPE publications and data supply. Importantly, the figures (`jcope-stats-jp.png` and `jcope-stats-en.png`) are embedded into the JCOPE website (https://www.jamstec.go.jp/jcope/htdocs/publication.html) and the html file (`public/jcope-publist.html`) is referred, so make sure they are up to date.

This notebook should be run every year (preferably in January) to update the figures with the information of the previous year. Here is the workflow for the annual update:

1. Ask Zhang-san for the total number of data supplied in the previous year (from January to December, not the financial year).
1. Manually update the reference list (`jcope-stats.bib`) by adding the new publications. To do this, I use Google Scholar, search for papers with keywords "jcope ocean model", filter the search result with the publication year, identify the relevant papers, add them to My Library (I have a label called "JCOPE"), export the updated Library as a Bibtex file, replace it with `jcope-stats.bib` on the source code.
1. Run the Python program (`python generate_html_references.py`) to update the publiation list (`public/jcope-publist.html`).
1. Add the new entry for the number of publications and data supply to the notebook (`jcope-stats.ipynb`). Run the notebook to update the figures. This will update the figures displayed on the above website automatically.